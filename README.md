# GameOfLife



## Task Description

I based my work on Conway's "Game of Life" - a zero player game, where they may only choose the starting points of 'cells' - living organisms. 

If a cell satisfies certain conditions it either becomes alive, dies or stays alive. 

## Implementation & Functions

For this task, I used Microsoft Visual Studio and Microsoft Forms, supplied by .NET. I used it instead of other GUI libraries because I was already familiar with it when I worked with C#.

Because I used Windows Forms, the project doesn't have a CMakeLists.txt file, so you need a Visual Studio to run my project.

To display cells I've chosen to use a DataGridView - a simple table for displaying data. To differentiate between alive and dead cells, the background color of cells get changed. Black - alive cell, white - dead cell.

User can input cell states in 2 ways - either directly clicking on the cells, while the simulation isn't running, or generate a random table by clicking the button.

User can watch simulation step by step using "Next Step" button, or run a simulation that updates the table every N milliseconds, N is chosen by the slider at the top.

## Thread differences

Using cycles can be pretty hard when working with GUI programs.

That's why I used a Background Worker - without it everything just freezes up, and you can't do anything.

So a thread was a great improvement for this project.