#pragma once
#include <cstdlib>

namespace CppCLRWinFormsProject
{
	using namespace System;
	using namespace ComponentModel;
	using namespace Collections;
	using namespace Windows::Forms;
	using namespace Data;
	using namespace Drawing;

    constexpr int tableWidth = 20;
    constexpr int tableHeight = 20;
    int delta = 300; //speed of the simulation
    int currentTable[tableWidth][tableHeight];
    int nextTable[tableWidth][tableHeight];

	/// <summary>
	/// Summary for Form1
	/// </summary>


	public ref class Form1 : public Form
	{
	public:
		Form1()
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		DataGridView^ dataGridView1;

	private:
		Button^ button1;

	private:
		Button^ button2;

	private:
		BackgroundWorker^ backgroundWorker1;

	private:
		Button^ button3;

	private:
		TrackBar^ trackBar1;

	private:
		Label^ label1;

	private:
		Label^ label2;

	private:
		Label^ label3;

	private:
		Label^ label4;

	private:
		Button^ button7;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent()
		{
			this->dataGridView1 = (gcnew DataGridView());
			this->button1 = (gcnew Button());
			this->button2 = (gcnew Button());
			this->backgroundWorker1 = (gcnew BackgroundWorker());
			this->button3 = (gcnew Button());
			this->trackBar1 = (gcnew TrackBar());
			this->label1 = (gcnew Label());
			this->label2 = (gcnew Label());
			this->label3 = (gcnew Label());
			this->label4 = (gcnew Label());
			this->button7 = (gcnew Button());
			(cli::safe_cast<ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<ISupportInitialize^>(this->trackBar1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->ColumnHeadersVisible = false;
			this->dataGridView1->Location = Point(26, 34);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->ScrollBars = ScrollBars::None;
			this->dataGridView1->Size = Drawing::Size(400, 400);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->CellClick += gcnew DataGridViewCellEventHandler(this, &Form1::dataGridView1_CellClick);
			// 
			// button1
			// 
			this->button1->Location = Point(469, 266);
			this->button1->Name = L"button1";
			this->button1->Size = Drawing::Size(127, 50);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Next Step";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = Point(469, 384);
			this->button2->Name = L"button2";
			this->button2->Size = Drawing::Size(127, 50);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Start";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew EventHandler(this, &Form1::button2_Click);
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->WorkerSupportsCancellation = true;
			this->backgroundWorker1->DoWork += gcnew DoWorkEventHandler(this, &Form1::backgroundWorker1_DoWork);
			// 
			// button3
			// 
			this->button3->Enabled = false;
			this->button3->Location = Point(469, 384);
			this->button3->Name = L"button3";
			this->button3->Size = Drawing::Size(127, 50);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Stop";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Visible = false;
			this->button3->Click += gcnew EventHandler(this, &Form1::button3_Click);
			// 
			// trackBar1
			// 
			this->trackBar1->Location = Point(469, 56);
			this->trackBar1->Maximum = 500;
			this->trackBar1->Minimum = 1;
			this->trackBar1->Name = L"trackBar1";
			this->trackBar1->RightToLeft = Windows::Forms::RightToLeft::No;
			this->trackBar1->Size = Drawing::Size(145, 45);
			this->trackBar1->TabIndex = 4;
			this->trackBar1->Value = 300;
			this->trackBar1->Scroll += gcnew EventHandler(this, &Form1::trackBar1_Scroll);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = Point(473, 34);
			this->label1->Name = L"label1";
			this->label1->Size = Drawing::Size(87, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"Simulation speed";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = Point(466, 88);
			this->label2->Name = L"label2";
			this->label2->Size = Drawing::Size(33, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"faster";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = Point(578, 88);
			this->label3->Name = L"label3";
			this->label3->Size = Drawing::Size(37, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"slower";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = Point(33, 18);
			this->label4->Name = L"label4";
			this->label4->Size = Drawing::Size(163, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Click on a cell to change its state";
			// 
			// button7
			// 
			this->button7->Location = Point(469, 147);
			this->button7->Name = L"button7";
			this->button7->Size = Drawing::Size(127, 56);
			this->button7->TabIndex = 13;
			this->button7->Text = L"Randomly fill in the table";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew EventHandler(this, &Form1::button7_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = SizeF(6, 13);
			this->AutoScaleMode = Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = Drawing::Size(620, 454);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->trackBar1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"Form1";
			this->Text = L"Game of Life";
			this->Load += gcnew EventHandler(this, &Form1::Form1_Load);
			(cli::safe_cast<ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<ISupportInitialize^>(this->trackBar1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();
		}
#pragma endregion

	private:
		Void button1_Click(Object^ sender, EventArgs^ e)
		{
			nextStep();
		}
		
		Void Form1_Load(Object^ sender, EventArgs^ e)
		{
			dataGridView1->ColumnCount = tableWidth;
			dataGridView1->RowCount = tableHeight;
			dataGridView1->AutoResizeRows(DataGridViewAutoSizeRowsMode::AllCellsExceptHeaders);
			dataGridView1->AutoResizeColumns(DataGridViewAutoSizeColumnsMode::AllCellsExceptHeader);
			for (int i = 0; i < tableWidth; i++)
			{
				for (int j = 0; j < tableHeight; j++)
				{
					currentTable[i][j] = 0;
					nextTable[i][j] = 0;
				}
			}
		}
		
		Void dataGridView1_CellClick(Object^ sender, DataGridViewCellEventArgs^ e)
		{
			if (currentTable[e->RowIndex][e->ColumnIndex] == 0)
			{
				currentTable[e->RowIndex][e->ColumnIndex] = 1;
				dataGridView1->Rows[e->RowIndex]->Cells[e->ColumnIndex]->Style->BackColor = BackColor.Black;
			}
			else
			{
				currentTable[e->RowIndex][e->ColumnIndex] = 0;
				dataGridView1->Rows[e->RowIndex]->Cells[e->ColumnIndex]->Style->BackColor = BackColor.White;
			}
		}
		
		void nextStep()
		{
			calculateNext();
			for (int i = 0; i < tableWidth; i++)
			{
				for (int j = 0; j < tableHeight; j++)
				{
					if (nextTable[i][j] == 1)
					{
						dataGridView1->Rows[i]->Cells[j]->Style->BackColor = BackColor.Black;
					}
					else
					{
						dataGridView1->Rows[i]->Cells[j]->Style->BackColor = BackColor.White;
					}
					currentTable[i][j] = nextTable[i][j];
				}
			}
		}
		
		void calculateNext()
		{
			for (int i = 0; i < tableWidth; i++)
			{
				for (int j = 0; j < tableHeight; j++)
				{
					nextTable[i][j] = willBeAlive(numberOfNeighbours(i, j), currentTable[i][j]);
				}
			}
		}
		
		int static willBeAlive(int numberOfNeighbours, int cellState)
		{
			if (cellState == 1)
			{
				if (numberOfNeighbours == 2 || numberOfNeighbours == 3)
				{
					return 1;
				}
				return 0;
			}
			if (numberOfNeighbours == 3)
			{
				return 1;
			}
			return 0;
		}
		
		int numberOfNeighbours(int row, int column)
		{
			if (row == 0 && column == 0) //top left cell
			{
				return currentTable[row + 1][column + 1] + currentTable[row][column + 1] + currentTable[row + 1][
					column];
			}
			if (row == tableHeight - 1 && column == tableWidth - 1) //bottom right cell
			{
				return currentTable[row - 1][column - 1] + currentTable[row][column - 1] + currentTable[row - 1][
					column];
			}
			if (row == tableHeight - 1 && column == 0) //bottom left cell
			{
				return currentTable[row - 1][column] + currentTable[row][column + 1] + currentTable[row - 1][column +
					1];
			}
			if (row == 0 && column == tableWidth - 1) //top right cell
			{
				return currentTable[row - 1][column - 1] + currentTable[row][column + 1] + currentTable[row - 1][
					column];
			}
			if (row == 0) //top row
			{
				return currentTable[row][column - 1] + currentTable[row][column + 1] +
					currentTable[row + 1][column - 1] + currentTable[row + 1][column] + currentTable[row + 1][column +
						1];
			}
			if (row == tableHeight - 1) //bottom row
			{
				return currentTable[row - 1][column - 1] + currentTable[row - 1][column] + currentTable[row - 1][column
						+ 1] +
					currentTable[row][column - 1] + currentTable[row][column + 1];
			}
			if (column == 0) //first column
			{
				return currentTable[row - 1][column] + currentTable[row - 1][column + 1] +
					currentTable[row][column + 1] +
					currentTable[row + 1][column] + currentTable[row + 1][column + 1];
			}
			if (column == tableWidth - 1) //last column
			{
				return currentTable[row - 1][column - 1] + currentTable[row - 1][column] +
					currentTable[row][column - 1] +
					currentTable[row + 1][column - 1] + currentTable[row + 1][column];
			}
			//everything else
			return currentTable[row - 1][column - 1] + currentTable[row - 1][column] + currentTable[row - 1][column + 1]
				+
				currentTable[row][column - 1] + currentTable[row][column + 1] +
				currentTable[row + 1][column - 1] + currentTable[row + 1][column] + currentTable[row + 1][column + 1];
		}
		
		Void button2_Click(Object^ sender, EventArgs^ e)
		{
			button2->Visible = false;
			button3->Visible = true;
			button3->Enabled = true;
			button1->Enabled = false;
			label4->Visible = false;
			trackBar1->Enabled = false;
			dataGridView1->Enabled = false;
			backgroundWorker1->RunWorkerAsync(2);
		}
		
		Void backgroundWorker1_DoWork(Object^ sender, DoWorkEventArgs^ e)
		{
			while (true)
			{
				if (backgroundWorker1->CancellationPending)
				{
					e->Cancel = true;
					break;
				}
				nextStep();
				Threading::Thread::Sleep(delta);
			}
		}
		Void button3_Click(Object^ sender, EventArgs^ e)
		{
			button3->Visible = false;
			button2->Visible = true;
			button1->Enabled = true;
			dataGridView1->Enabled = true;
			trackBar1->Enabled = true;
			label4->Visible = true;
			backgroundWorker1->CancelAsync();
		}
		Void trackBar1_Scroll(Object^ sender, EventArgs^ e)
		{
			delta = trackBar1->Value;
		}

		Void button7_Click(Object^ sender, EventArgs^ e)
		{
			for (int i = 0; i < tableWidth; i++)
			{
				for (int j = 0; j < tableHeight; j++)
				{
					const int v = rand() % 10 + 1;
					if (v > 5)
					{
						currentTable[i][j] = 1;
						dataGridView1->Rows[i]->Cells[j]->Style->BackColor = BackColor.Black;
					}
					else
					{
						currentTable[i][j] = 0;
						dataGridView1->Rows[i]->Cells[j]->Style->BackColor = BackColor.White;
					}
				}
			}
		}
	};
}
